# asterius-nix-usage-template

this repository is an example for a asterius project. it uses the nix expression [asterius-nix](https://gitlab.com/pauldennis/asterius-nix) to compile the [hilbert example](https://www.tweag.io/blog/2019-12-19-asterius-diagrams/).

# usage

install `nix`. see [Nix Manual](https://nixos.org/manual/nix/stable/) for guidance. install `niv`. see [Towards reproducibility: Pinning nixpkgs](https://nixos.org/guides/towards-reproducibility-pinning-nixpkgs.html) for introduction.

update the dependencies:

```
niv update
```

build the example:

```
nix-build --show-trace ./default.nix
```
