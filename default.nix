{ sources ? import ./nix/sources.nix
, pkgs ? import sources.nixpkgs {}

, stdenv ? pkgs.stdenv

, asterius ? import sources.asterius

, toBeCompiled ? ./soureceCode
, additionalCabalPackages ? ./additionalCabalPackages

}:


stdenv.mkDerivation {
  name = "asterius-example";
  builder = ./builder.sh;

  buildingTheThing = asterius
    {
      inherit toBeCompiled;
      inherit additionalCabalPackages;
    };

}
